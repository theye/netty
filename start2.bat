@echo off
set LIB_JARS="F:\netty\lib\*"
set RUN_JARS="F:\netty\target\classes"
java -Dfile.encoding=UTF-8  ^
-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./prof/netty.hprof ^
-classpath %RUN_JARS%;%LIB_JARS%  ^
-Xmx50m ^
com.xcc.core.oom.OomTest
goto end