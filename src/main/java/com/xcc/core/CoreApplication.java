package com.xcc.core;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.xcc.core.config.PropertiesUtil;
import com.xcc.core.netty.NettyLengthServer;

@SpringBootApplication
public class CoreApplication {
    
    
    private static final Logger logger=LoggerFactory.getLogger(CoreApplication.class);
    
    public static NettyLengthServer mServer=new NettyLengthServer();
    
    public static NettyLengthServer getServrt() {
        return mServer;
    }
    
   	public static void main(String[] args) {
		SpringApplication.run(CoreApplication.class, args);
	    try {
	        getServrt().bind(PropertiesUtil.getIntValue("server.port"));
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
	}
   	
}
