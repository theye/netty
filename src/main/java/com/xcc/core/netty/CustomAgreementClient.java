package com.xcc.core.netty;

import com.xcc.core.netty.client.HeartBeatClientRespHandler;
import com.xcc.core.netty.coder.NettyMessageDecoder;
import com.xcc.core.netty.coder.NettyMessageEncoder;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

public class CustomAgreementClient {



    private final String host;
    private final int port;
    private Channel channel;

    //连接服务端的端口号地址和端口号
    public CustomAgreementClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group).channel(NioSocketChannel.class)  // 使用NioSocketChannel来作为连接用的channel类
        //  .option(ChannelOption.TCP_NODELAY, true)
        .handler(new ChannelInitializer<SocketChannel>() { // 绑定连接初始化器
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                System.out.println("正在连接中...");
                ch.pipeline().addLast( new NettyMessageDecoder(1024 * 1024, 4, 4));
                ch.pipeline().addLast("MessageEncoder", new NettyMessageEncoder());
              //  ch.pipeline().addLast("readTimeoutHandler", new ReadTimeoutHandler(50));
                ch.pipeline().addLast(new HeartBeatClientRespHandler()); //客户端处理类
            }
            
        });
        //发起异步连接请求，绑定连接端口和host信息
        final ChannelFuture future = b.connect(host, port).sync();

        future.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture arg0) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("连接服务器成功");

                } else {
                    System.out.println("连接服务器失败");
                    future.cause().printStackTrace();
                    group.shutdownGracefully(); //关闭线程组
                }
            }
        });
        this.channel = future.channel();
    }

    public Channel getChannel() {
        return channel;
    }
    public static void main(String[] args) throws Exception {
        new CustomAgreementClient("127.0.0.1",10087).start();
    }
 

}
