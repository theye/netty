package com.xcc.core.netty.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

/**
 * 版权： 
 * <简述>
 * <详细描述>
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public interface Action {
    
    public boolean read(String key);
    boolean write(String key, ByteBuf bs);
    boolean writeClient(String key, ByteBuf bs);
}
