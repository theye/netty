package com.xcc.core.netty.client;

import com.xcc.core.netty.manager.ManagerChannel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;


/**
 * 
 * 版权： RPC写和读
 * <简述>
 * <详细描述>
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public class ClientUtil implements Action{

    
    
    @Override
    public boolean write(String key,ByteBuf buf) {
        byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        buf.writeBytes(b);
        System.out.println("发送给服务器数据：");
        for(byte a:b) {
            System.out.print(a+" ");
        }
        System.out.println();
        Channel ch=ManagerChannel.getServerChannel(key);
        ch.writeAndFlush(buf);
        return true;
    }

    @Override
    public boolean read(String key) {
       
        return false;
    }

    @Override
    public boolean writeClient(String key, ByteBuf buf) {
       byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        buf.writeBytes(b);
        System.out.print("发送给代理客户端"+key+":");
           for(byte a:b) {
               System.out.print(a+" ");
           }
           System.out.println();
        Channel ch=ManagerChannel.getServerChannel(key);
        ch.writeAndFlush(buf);
        return true;
    }

    
    
    public boolean writeServer(String key, ByteBuf buf) {
       byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        buf.writeBytes(b);
        System.out.print("发送给服务器发送客户端"+key+":");
           for(byte a:b) {
               System.out.print(a+" ");
           }
        System.out.println();
        Channel ch=ManagerChannel.getClientChannel(key);
        ch.writeAndFlush(buf);
        return true;
    }
    
    
    
    
    public boolean writePro(String key,ByteBuf buf) {
        byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        buf.writeBytes(b);
        for(byte a:b) {
            System.out.print(a+" ");
        }
        System.out.println();
        Channel ch=ManagerChannel.getServerChannel(key);
        ch.writeAndFlush(buf);
        return true;
    }

}
