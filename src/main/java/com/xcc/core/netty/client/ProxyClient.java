package com.xcc.core.netty.client;

import java.io.InputStream;
import java.net.SocketAddress;

import io.netty.channel.ChannelHandlerContext;

public class ProxyClient  extends Client{

    public ProxyClient(String host, int port) {
        super(host, port);
    }

    @Override
    public InputStream getInputStream(ChannelHandlerContext ctx, byte[] bs) {
        SocketAddress socketServer=ctx.channel().remoteAddress();
        System.out.println(socketServer);
        return null;
    }

    public static void main(String[] args) throws Exception {
      Client client=  new ProxyClient("127.0.0.1", 10089);
      client.connect();
   
      
    }
    
}
