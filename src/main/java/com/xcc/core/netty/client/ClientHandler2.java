package com.xcc.core.netty.client;




import com.xcc.core.netty.NettyClient;
import com.xcc.core.netty.manager.ManagerChannel;
import com.xcc.core.util.ZkClientUitl;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ClientHandler2 extends ChannelInboundHandlerAdapter{
    
    ClientUtil mClientUtil=new ClientUtil();

    //创建管理通道
    @Override
    public void channelActive(ChannelHandlerContext ctx)
        throws Exception {
        String s="asdad";
        ByteBuf message=Unpooled.buffer(s.length());
        message.writeBytes(s.getBytes());
        ctx.writeAndFlush(message);
        ManagerChannel.putSeverChannel(ctx.channel());
       // 10087
        ZkClientUitl.createEphemeral("/server/"+ctx.channel().id().asLongText(),"".getBytes());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
        throws Exception {
        System.out.print("模拟客户端接收端到数据:");
        ByteBuf buf=   (ByteBuf)msg;
        byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        for(byte a:b) {
            System.out.print(a+" ");
        }
        System.out.println();
        ByteBuf b2=  Unpooled.copiedBuffer("xxxx".getBytes());
        ctx.writeAndFlush(b2);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
                                Throwable cause)
        throws Exception {
        System.out.println("服务器连接异常");
        ManagerChannel.removeServer(ctx.channel());
        new NettyClient("127.0.0.1",10087).start();
        super.exceptionCaught(ctx, cause);
    }

   
}