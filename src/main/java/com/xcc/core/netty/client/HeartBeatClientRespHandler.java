/*
 * Copyright 2013-2018 Lilinfeng.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xcc.core.netty.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xcc.core.netty.model.Header;
import com.xcc.core.netty.model.NettyMessage;


/**
 * @author xcc
 * @date 2020年12月21日
 * @version 1.0
 */
public class HeartBeatClientRespHandler extends ChannelInboundHandlerAdapter {

	private static final Log LOG = LogFactory.getLog(HeartBeatClientRespHandler.class);


    @Override
    public void channelActive(ChannelHandlerContext ctx)
        throws Exception {
        NettyMessage mNettyMessage=new NettyMessage();
        Header header=new Header();
        mNettyMessage.setHeader(header);
        mNettyMessage.setBody("A".getBytes());
        ctx.writeAndFlush(mNettyMessage);
    }







    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
        throws Exception {
        ByteBuf b=(ByteBuf)msg;
        byte[] bs=new byte[b.readableBytes()];
        b.readBytes(bs);
        System.out.print("接收：");
        for(byte b1:bs) {
            System.out.print(b1+" ");
        }
        
    }


    

}
