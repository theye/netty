package com.xcc.core.netty.server;

import com.xcc.core.netty.client.ClientUtil;
import com.xcc.core.netty.manager.ManagerChannel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ServerHandler extends ChannelInboundHandlerAdapter {

    ClientUtil mClientUtil=new ClientUtil();

    
    
    @Override
    public void channelActive(ChannelHandlerContext ctx)
        throws Exception {
        System.out.println(ctx.channel().id().asShortText());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
        throws Exception {
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
        throws Exception {
        ManagerChannel.removeClient(ctx.channel());
        if(ctx.channel().isActive()) {
            System.out.println(ctx.channel().id().asLongText()+"关闭");
            ctx.close();
        }
        super.exceptionCaught(ctx, cause);
    }







}


