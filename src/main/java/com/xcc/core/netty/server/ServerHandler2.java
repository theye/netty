package com.xcc.core.netty.server;

import com.xcc.core.netty.client.ClientUtil;
import com.xcc.core.netty.manager.ManagerChannel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ServerHandler2 extends ChannelInboundHandlerAdapter {

    ClientUtil mClientUtil=new ClientUtil();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
        throws Exception {
        System.out.print("3390服务器接收到数据：");
        ByteBuf buf=   (ByteBuf)msg;
        byte[] b=new byte[buf.readableBytes()];
        buf.readBytes(b);
        for(byte a:b) {
            System.out.print(a+" ");
        }
        System.out.println();
        ByteBuf b2=  Unpooled.copiedBuffer("xxxx222".getBytes());
        ctx.writeAndFlush(b2);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
        throws Exception {
        ManagerChannel.removeClient(ctx.channel());
        if(ctx.channel().isActive()) {
            System.out.println(ctx.channel().id().asLongText()+"关闭");
            ctx.close();
        }
        super.exceptionCaught(ctx, cause);
    }







}


