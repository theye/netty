package com.xcc.core.netty.server;

import java.io.InputStream;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

public class HttpProx extends Server {

    private static final Log logger = LogFactory.getLog(HttpProx.class);

    
    @Override
    public InputStream getInputStream(ChannelHandlerContext ctx, Object msg) {

        if (msg instanceof HttpRequest) {
            HttpRequest request = (HttpRequest) msg;
            try {
                System.out.println(request.getUri());
                String ul=request.getUri();
                URL url;
                if(ul.split(":")[1].equals("443")) {
                      url=new URL("https://"+request.getUri());
                }else {
                    url=new URL("http://"+request.getUri());
                }
                InputStream is = url.openStream();
                byte[] bs=new byte[1024*1024];
                is.read(bs);
           //     logger.debug();  
                
                FullHttpResponse response = new DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1, HttpResponseStatus.OK, Unpooled.wrappedBuffer(new String(bs) != null ? bs : new byte[0]));
            response.headers().set("CONTENT_TYPE", "text/html");
            response.headers().set("CONTENT_LENGTH",
                    response.content().readableBytes());
            response.headers().set("CONNECTION", HttpHeaderValues.KEEP_ALIVE);
            ctx.write(response);
            ctx.flush();
                
                
                
                
        //        ctx.channel().writeAndFlush(bs);
            } catch ( Exception e) {
                e.printStackTrace();
            }
          
        }
        
     
      //  getOutStream(null);
        /*else {
            ByteBuf byteBuf = (ByteBuf) msg;
            System.out.println(new Date() + ": 服务端读到数据 -> " + byteBuf.toString(Charset.forName("utf-8")));
        }*/

        return null;
    }

    public static void main(String[] args) throws Exception {
        HttpProx mHttpProx=new  HttpProx();
        mHttpProx.bind(10089);
    }

    //自定义编码类型
    @Override
    public void getHandles(SocketChannel sh) {
        sh.pipeline().addLast(
            new HttpResponseEncoder());
        // server端接收到的是httpRequest，所以要使用HttpRequestDecoder进行解码
        sh.pipeline().addLast(
            new HttpRequestDecoder()); 
    }

}
