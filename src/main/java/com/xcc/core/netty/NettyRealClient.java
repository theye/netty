package com.xcc.core.netty;


import java.io.OutputStream;
import java.net.Socket;

import com.xcc.core.netty.client.RealChannelHandler;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class NettyRealClient {
        
    
    private final String host;
    private final int port;
    private Channel channel;
 
    //连接服务端的端口号地址和端口号
    public NettyRealClient(String host, int port) {
        this.host = host;
        this.port = port;
    }
 
    public void start() throws Exception {
          EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group).channel(NioSocketChannel.class)  // 使用NioSocketChannel来作为连接用的channel类
      //  .option(ChannelOption.TCP_NODELAY, true)
        .handler(new ChannelInitializer<SocketChannel>() { // 绑定连接初始化器
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    System.out.println("正在连接中...");
                    ChannelPipeline pipeline = ch.pipeline();
              //      pipeline.addLast(new ShenSuDecoder()); //编码request
                 //      pipeline.addLast(new ShenSuEncoder()); //编码request
                   // 添加一个出站的 handler 对数据进行编码
                   //pipeline.addLast("encoder", new StringEncoder());
              //     pipeline  .addLast(new StringEncoder()); //解码request
              //      pipeline  .addLast(new StringDecoder()) ;
               //     pipeline.addLast(new FixedLengthFrameDecoder(20));
                //    pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4)); 
                 //   pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
                    pipeline.addLast(new RealChannelHandler()); //客户端处理类
                }
            });
        //发起异步连接请求，绑定连接端口和host信息
        final ChannelFuture future = b.connect(host, port).sync();
 
        future.addListener(new ChannelFutureListener() {
 
            @Override
            public void operationComplete(ChannelFuture arg0) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("连接服务器成功");
                } else {
                    System.out.println("连接服务器失败");
                 //   future.cause().printStackTrace();
               //     group.shutdownGracefully(); //关闭线程组
                }
            }
        });
        this.channel = future.channel();
    }
 
    public Channel getChannel() {
        return channel;
    }
       public static void main(String[] args) throws Exception {
          /* NettyClient m=new NettyClient(System.getProperty("server.ip")
               ,Integer.parseInt(System.getProperty("server.port")));
           m.start();
           System.out.println(System.getProperty("server.ip"));*/
          // 10087
    	//   new NettyRealClient("127.0.0.1",3389).start();
          // new NettyRealClient("115.227.0.241",10088).start();
           Socket mSocket=new Socket("115.227.0.241",10088);
           OutputStream  out= mSocket.getOutputStream();
           out.write("asda".getBytes());
           out.flush();
       }
       
       
       
     
       
}
