package com.xcc.core.netty.model.emua;


public  enum Anum {
 //   a(0),b(1),c(2),d(3),e(4);
 //   15-五针法、14-四针法、03-暴露前三针法、13-暴露后三针法、12-二针法
    rule15("15","五针法"),
    rule14("14","四针法"),
    rule03("03","暴露前三针法"),
    rule13("13","暴露后三针法"),
    rule12("12","二针法");
    private   String name;
    private  String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    Anum(String value, String name) {
       this.name=name;
       this.value=value;
    }
 
    public String getValue(String value) {
        return this.name;
    }
    
 
    public static void main(String[] args) {
      //  for(int i=0;i<Anum.values().length;i++) {
           // System.out.println(Anum.getName("03"));
     //   }
    }
}
