package com.xcc.core.netty.model;

public class LengthModel {
    
    private int length;
    
    private int id;
    
    private byte[] bs;
    
    
    private byte type;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public byte[] getBs() {
        return bs;
    }

    public void setBs(byte[] bs) {
        this.bs = bs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}
    
    
}
