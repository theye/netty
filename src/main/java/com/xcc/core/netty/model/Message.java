package com.xcc.core.netty.model;

public class Message {
    private String send;
    
    private String received;
    
    private String type;//1.文本2.图片3. 视频
    
    
    private byte[] message;

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public byte[] getMessage() {
        return message;
    }

    public void setMessage(byte[] message) {
        this.message = message;
    }
    
}
