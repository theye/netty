package com.xcc.core.netty.coder;

import java.util.List;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
//编码
public class ShenSuDecoder extends MessageToMessageDecoder<byte[]>{

    @Override
    protected void decode(ChannelHandlerContext ctx, byte[] msg, List<Object> out)
        throws Exception {
        System.out.println(msg);
    }
    public   int toInt(byte[] bytes){
        int number = 0;
        for(int i = 0; i < 4 ; i++){
            number += bytes[i] << i*8;
        }
        return number;
    }
}
