package com.xcc.core.netty.coder;

import com.xcc.core.netty.model.LengthModel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class FixedLengthFrameDecoder extends LengthFieldBasedFrameDecoder {
    
    public FixedLengthFrameDecoder() {
        super(1024*1024,5,4,0,0);
    }

    @Override
    protected LengthModel decode(ChannelHandlerContext ctx, ByteBuf in2)
        throws Exception {
        ByteBuf in = (ByteBuf) super.decode(ctx, in2);
        
        LengthModel    mLengthModel =new LengthModel();
        if (in == null) {
            return null;
        }
      
        if (in.readableBytes() < 4) {
            return null;
        }
        int id = in.readInt();
        mLengthModel.setId(id);
        mLengthModel.setType(in.readByte());
        int frameLength = in.readInt();
        mLengthModel.setLength(frameLength);
        byte[] data = new byte[frameLength];
        in.readBytes(data);
        mLengthModel.setBs(data);
        in.release();
        return mLengthModel;
    }

     
}
