package com.xcc.core.netty.coder;


import com.xcc.core.netty.model.LengthModel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 编码
 * <简述>
 * <详细描述>
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public class FixedLengthFrameEncoder extends MessageToByteEncoder<LengthModel>{

    // 后面添加内容  length+data
    @Override
    protected void encode(ChannelHandlerContext ctx, 
                          LengthModel msg, ByteBuf out)
        throws Exception {
        out.writeInt(msg.getId());
        out.writeByte(msg.getType());
        if (msg.getBs()!= null&&msg.getLength()>0) {
            out.writeInt(msg.getLength());
        } else {
            out.writeInt(0);
        }
        out.writeBytes(msg.getBs());
    }

}
