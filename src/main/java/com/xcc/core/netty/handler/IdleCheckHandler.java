package com.xcc.core.netty.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcc.core.netty.model.LengthModel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;

public class IdleCheckHandler  extends IdleStateHandler {

    
    public static final int USER_CHANNEL_READ_IDLE_TIME = 1200;

    public static final int READ_IDLE_TIME = 60;

    public static final int WRITE_IDLE_TIME = 40;

    private static Logger logger = LoggerFactory.getLogger(IdleCheckHandler.class);

    public IdleCheckHandler(int readerIdleTimeSeconds, int writerIdleTimeSeconds, int allIdleTimeSeconds) {
        super(readerIdleTimeSeconds, writerIdleTimeSeconds, allIdleTimeSeconds);
    }

    @Override
    protected void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) throws Exception {
         	LengthModel mLengthModel=new LengthModel();
         	mLengthModel.setType((byte)2);
         	byte[] bs="I am alive".getBytes();
        	mLengthModel.setBs(bs);
         	mLengthModel.setLength(bs.length);
         	ctx.writeAndFlush(mLengthModel);
    }

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		super.exceptionCaught(ctx, cause);
		logger.debug("链接中断");
	}
    
    
    
    
    
    
}