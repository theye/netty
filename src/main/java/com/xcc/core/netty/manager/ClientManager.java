package com.xcc.core.netty.manager;

import com.xcc.core.config.PropertiesUtil;
import com.xcc.core.netty.client.Client;
import com.xcc.core.netty.client.ProxyClient;

public class ClientManager {
    
    
    public static Client getClient(String host, int port) {
        return new ProxyClient(host,port);
    }
    
    public static Client getClient() {
        return new ProxyClient(PropertiesUtil.getValue("host"),PropertiesUtil.getIntValue("port"));
    }
    
    
}
