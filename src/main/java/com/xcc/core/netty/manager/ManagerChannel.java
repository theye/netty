package com.xcc.core.netty.manager;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import io.netty.channel.Channel;

/**
 * 版权： xcc
 * <简述>
 * <详细描述> 管理通道类
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public class ManagerChannel {
    //10088
    public static final Map<String,Channel> mServer1=new  Hashtable();
    //3389
    public static final Map<String,Channel> mServer2=new  Hashtable();

    //内部通信管理
    public static final Map<String,Channel> mClient1=new  Hashtable();
    //外部通信管理
    public static final Map<String,Channel> mClient2=new  Hashtable();
    //内外部通道
    public static final Map<String,String> channelMapper=new  Hashtable();
    
    
    
    //(send+userId)+(received+userId)
    public static final Map<String,String> mUserChannel=new  Hashtable();
    //serverId+userIds
    public static final Map<String,List<String>> mGroupChannel=new  Hashtable();

    public static Map<String,Channel> getAllClient() {
        return mClient1;
    }
    public static Map<String,Channel> getAllClient2() {
        return mClient2;
    }
    public static Map<String,Channel> getAllServer() {
        return mServer1;
    }
    public static Map<String,Channel> getAllServer2() {
        return mServer2;
    }
    public static Channel getServerChannel(String key) {
        return  mServer1.get(key);
    }
    public static Channel getServerChannel2(String key) {
        return  mServer2.get(key);
    }
    public static void  putSeverChannel(Channel ch) {
        mServer1.put(ch.id().asLongText(),ch);
    }
    public static void  putSeverChannel2(Channel ch) {
        mServer2.put(ch.id().asLongText(),ch);
    }
    public static void  removeServer(Channel ch) {
        mServer1.remove(ch.id().asLongText());
    }
    public static void  removeServer2(Channel ch) {
        mServer2.remove(ch.id().asLongText());
    }
    public static Channel getClientChannel(String key) {
        return  mClient1.get(key);
    }
    public static void  putClientChannel(Channel ch) {
        mClient1.put(ch.id().asLongText(),ch);
    }
    public static void  removeClient(Channel ch) {
        mClient1.remove(ch.id().asLongText());
    }
    public static void  removeClient(String  key) {
        mClient1.remove(key);
    }
    public static Channel getClientChannel2(String key) {
        return  mClient2.get(key);
    }
    public static void  putClientChannel2(Channel ch) {
        mClient2.put(ch.id().asLongText(),ch);
    }
    public static void  removeClient2(Channel ch) {
        mClient2.remove(ch.id().asLongText());
    }
    public static void  removeClient2(String  key) {
        mClient2.remove(key);
    }
    public static Map<String, String> getChannelmapper() {
        return channelMapper;
    }
    
    public static Channel getChannel2(String name) {
        return mClient2.get(channelMapper.get(name));
    }

    public static Channel getChannelServer2(String name) {
        return mServer2.get(channelMapper.get(name));
    }
    
}
