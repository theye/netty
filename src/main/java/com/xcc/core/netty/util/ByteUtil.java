package com.xcc.core.netty.util;





/**
 * 版权：(C) 版权所有 2019-2022  苏州沈苏自动化有限公司
 * <简述>
 * <详细描述>
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public class ByteUtil {

    
    public static byte[] combine(String st,String msg) throws Exception {
        int[] end =new int[] { 0xef, 0xbc,  0xa0,  0xe6 ,  0xe8,  0x8b ,  0x8f,   0x83};
        byte[] sendMsg=msg.getBytes("utf-8");
        if(sendMsg.length>Integer.MAX_VALUE) {
            throw new Exception("发送内容过长");
        }
        byte[] header=new byte[76];
        byte[] length=new byte[4];
       byte[] lengthT=toLH(sendMsg.length);
       for(int i=0;i<lengthT.length;i++) {
           length[i]=lengthT[i];
       }
       byte[] endBytes=new byte[8];
       for(int i=0;i<end.length;i++) {
           endBytes[i]=(byte)end[i];
       }
        //head
       byte[] head=st.getBytes();
       for(int i=0;i<76;i++) {
           if(i<head.length) {
               header[i]=head[i];
           }else {
              break;
           }
        
       }
    
     byte[] send=addBytes( addBytes(
         addBytes(header,length),sendMsg ),endBytes);
     
      for(byte b:send) { System.out.print(Integer.toHexString((b&0xFF))+" "); }
       return send;
    }
    
    
    
    
    public static byte[] toLH(int n) {  
        byte[] b = new byte[4];  
        b[0] = (byte) (n & 0xff);  
        b[1] = (byte) (n >> 8 & 0xff);  
        b[2] = (byte) (n >> 16 & 0xff);  
        b[3] = (byte) (n >> 24 & 0xff);  
        return b;  
      }
    public static byte[] addBytes(byte[] data1, byte[] data2) { 
        byte[] data3 = new byte[data1.length + data2.length]; 
        System.arraycopy(data1, 0, data3, 0, data1.length); 
        System.arraycopy(data2, 0, data3, data1.length, data2.length); 
        return data3; 
       } 
    
       /*
        * public static void main(String[] args) throws Exception { SocketPool mSocketPool =new
        * SocketPool("127.0.0.1","10087"); String a="��"; DataOutputStream outputStream = new
        * DataOutputStream(mSocketPool.getSocketOutput());
        * outputStream.write(ByteUtil.combine("YMY_QH_START",a)); outputStream.flush(); }
        */
}
