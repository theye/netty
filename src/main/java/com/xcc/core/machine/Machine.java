package com.xcc.core.machine;

import java.io.File;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;

public class Machine {

	public static void main(String[] args) throws Exception {
	    //加载待读取图片
        File imageFile = new File("E:\\netty\\src\\main\\resources\\img\\340EB333492D87158175EDFC0D2BC4A5.png");
        //创建tess对象
        ITesseract instance = new Tesseract();
        //设置训练文件目录
       instance.setDatapath("C:\\Program Files\\Tesseract-OCR\\tessdata");
        //设置训练语言
    //     instance.setLanguage("chi_sim");
       instance.setLanguage("osd");
        //执行转换
        String result = instance.doOCR(imageFile);        
	}
	
}
