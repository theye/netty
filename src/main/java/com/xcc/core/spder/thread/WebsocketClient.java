package com.xcc.core.spder.thread;

import java.net.URI;
import java.util.concurrent.CountDownLatch;

public class WebsocketClient  implements   SpaderInterface{
    CountDownLatch    latch = new CountDownLatch(1);
    @Override
    public String excetor(String... strings)
        throws Exception {
        WebSocketClientInterFac myClient = new WebSocketClientInterFac(new URI("wss://hq.sinajs.cn/wskt?list="+strings[0]),latch);
        myClient.connect();
        latch.await();
        return myClient.getMessage();
    }
    
    
    public static void main(String[] args) throws Exception {
        WebsocketClient mWebsocketClient=new WebsocketClient();
       System.out.println(mWebsocketClient.excetor("sz300035"));
    }

}
