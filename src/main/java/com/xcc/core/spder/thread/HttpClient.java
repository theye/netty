package com.xcc.core.spder.thread;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HttpClient implements SpaderInterface{

    
    
    @Override
    public Object excetor(String... strings)
        throws Exception {
        Document mDocument=Jsoup.connect(strings[0])
            .ignoreContentType(false)
            .get();
        return mDocument.body();
    }

}
