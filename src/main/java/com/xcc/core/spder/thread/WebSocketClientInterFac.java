package com.xcc.core.spder.thread;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.FutureTask;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketClientInterFac extends WebSocketClient {
    Logger logger = LoggerFactory.getLogger(WebSocketClientInterFac.class);
       String message="";
       CountDownLatch latch;
    public WebSocketClientInterFac(URI serverUri) {
        super(serverUri);
    }

    public WebSocketClientInterFac(URI uri, CountDownLatch latch) {
        super(uri);
        this.latch=latch;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        logger.info("------ MyWebSocket onOpen ------");

    }

    @Override
    public void onMessage(String message) {
          this.message=message;
          this.close();
          this.latch.countDown();
    }
    @Override
    public void onClose(int code, String reason, boolean remote) {
        logger.info("------ MyWebSocket onClose ------");
    }

    @Override
    public void onError(Exception ex) {
        logger.info("------ MyWebSocket onError ------");
    }


    public static void main(String[] args) throws  Exception {
  //  String te=   new  WebSocketClientInterFac(new URI("wss://hq.sinajs.cn/wskt?list=sz000978")).excetor(args);
    //   System.out.println(te);
     
       //list=sz000978
       
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
 
 
}
