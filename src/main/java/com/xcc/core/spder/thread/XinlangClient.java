package com.xcc.core.spder.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import org.apache.commons.lang3.StringUtils;
import org.apache.xalan.xsltc.compiler.sym;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class XinlangClient implements SpaderInterface{

    Logger logger = LoggerFactory.getLogger(XinlangClient.class);

    private     String code;
    public XinlangClient() {
    }

    public XinlangClient(String string) {
        this.code=string;
    }



    @Override
    public String excetor(String... strings) throws  Exception {
      synchronized(code) {
            XinlangThread mHttpClient=new XinlangThread(this);
            if(strings.length>0) {
                setCode(strings[0]); 
            }
            //     System.out.println(getCode());
            FutureTask<String> futureTask = new FutureTask<String>(mHttpClient);
            new Thread(futureTask).start();
            return  futureTask.get();
       }
    }


    public static void main(String[] args) throws Exception {
        XinlangClient callable = new XinlangClient();
        callable.excetor();
        Thread.sleep(3000);
    }


    class XinlangThread implements Callable<String>{

        XinlangClient mXinlangClient;

        public XinlangThread(XinlangClient client) {
            this.mXinlangClient=client;
        }
        public   String call()  throws Exception {

            String urlsh="https://hq.sinajs.cn/etag.php?_="+System.currentTimeMillis()+"&list="+mXinlangClient.getCode();
            logger.debug(System.currentTimeMillis()+":"+
                Thread.currentThread().getName() + ":"
                + urlsh/*
                 * + "client:"+mXinlangClient
                 */);
            Document   mDocument = Jsoup.connect(urlsh)
                .ignoreContentType(true)
                .timeout(500000)
                .get();
            Element mElements= mDocument.body();
            String langchao=  mElements.ownText().split(",")[3];
            return langchao;
        }

    }
    public void setCode(String code) {
        this.code=code;
    }

    public String getCode() {
        return  this.code;
    }
}