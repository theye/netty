package com.xcc.core.spder.thread;

public class ThreadTest implements Runnable{

    private int  code=100;
    
    @Override
    public void run() {
        System.out.println("线程"+Thread.currentThread()
            .getName()+":"+  code);
    }
    
    public static void main(String[] args) {
        ThreadTest te=  new ThreadTest();
        Thread[] threads = new Thread[5];
        for(int i=0;i<5;i++) {
            threads[i]=new Thread(te,i+"");
        }
        for (Thread thread : threads) {
            thread.start();
        }
        
        
    }

   
    
}
