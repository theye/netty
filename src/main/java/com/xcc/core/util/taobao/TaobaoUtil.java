//package com.xcc.core.util.taobao;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import com.taobao.api.DefaultTaobaoClient;
//import com.taobao.api.TaobaoClient;
//@Component
//public class TaobaoUtil {
//    
//    
//    public     String api;
//    public     String key ;
//    public    String Secret;
//    
//    TaobaoClient client ;
//    
//    public TaobaoUtil(@Value("${taobao.api}") String api,
//                      @Value("${taobao.key}")String key ,
//                      @Value("${taobao.Secret}") String Secret) {
//         this.api=api;
//         this.key=key;
//         this.Secret=Secret;
//         System.out.println(api+":"+key+":"+Secret);
//        client = new DefaultTaobaoClient(api,key,Secret);
//    }
//
//    public TaobaoClient getClient() {
//        return client;
//    }
//
//    public void setClient(TaobaoClient client) {
//        this.client = client;
//    }
//}
