package com.xcc.core.util;

import org.I0Itec.zkclient.ZkClient;
//import org.junit.Test;

import com.xcc.core.config.PropertiesUtil;

/**
 * 版权
 * <简述>
 * <详细描述>后期+安全认证
 * @author   xcc
 * @version  $Id$
 * @since
 * @see
 */
public class ZkClientUitl {

    public static  ZkClient mZkClient =null ;

    static {
        System.out.println(PropertiesUtil.getValue("zookeeper.server"));
        mZkClient = new ZkClient(PropertiesUtil.getValue("zookeeper.server"));
     }
    
    public static ZkClient getZKClient() {
        return mZkClient;
    }
    //创建持续性节点
    public static void createNode(String path,byte[] bs) {
        mZkClient.createPersistent(path, bs);
    }
    //节点创建父节点 递归有的话报错
    public static void createNodeParent(String path,boolean flag) {
        mZkClient.createPersistent(path, flag);
    }
    //创建带序列号 create -s /test
    public static String createSerino(String path,byte[] bs) {
        return mZkClient.createPersistentSequential(path, bs);
    }
    //临时节点 +序号
    public static String createEphemeralSerino(String path,byte[] bs) {
       return  mZkClient.createEphemeralSequential(path, bs);
    }
    
    //临时节点 
    public static void createEphemeral(String path,byte[] bs) {
          mZkClient.createEphemeral(path, bs);
    }
    
    public static ZkClient getClient() {
        return mZkClient;
    }
    
    
   // @Test
    public void test() {
        ZkClientUitl.createEphemeralSerino("/xcc/xxx-", "".getBytes());
    }
    
    
    
}
