package com.xcc.core.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataUtil {

    /** 
     * 获取现在时间 
     * 
     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss 
     */  
  public static Date getNowDate() {  
     Date currentTime = new Date();  
     SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
     String dateString = formatter.format(currentTime);  
     ParsePosition pos = new ParsePosition(8);  
     Date currentTime_2 = formatter.parse(dateString, pos);  
     return currentTime_2;  
  }  
  
  
  public static Calendar setDate(int hour,int minute,int second) {
      Calendar calendar = Calendar.getInstance();  
      calendar.set(Calendar.HOUR_OF_DAY, hour);     // 控制时  
      calendar.set(Calendar.MINUTE,minute);       // 控制分  
      calendar.set(Calendar.SECOND, second);       // 控制秒  
      return calendar;
  }
  
  
  public static Date customeDate(int hour,int minute,int second) {
      Calendar calendar = Calendar.getInstance();  
      calendar.set(Calendar.HOUR_OF_DAY, hour);     // 控制时  
      calendar.set(Calendar.MINUTE,minute);       // 控制分  
      calendar.set(Calendar.SECOND, second);       // 控制秒  
      return calendar.getTime();
  }

  public static int dayForWeek() throws Exception {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      Calendar c = Calendar.getInstance();
      c.setTime(new Date());
      int dayForWeek = 0;
      if (c.get(Calendar.DAY_OF_WEEK) == 1) {
          dayForWeek = 7;
      } else {
          dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
      }
      return dayForWeek;
  }

  
}
