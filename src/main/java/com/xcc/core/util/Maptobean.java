package com.xcc.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class Maptobean {
    
    public static Object tran(Map map,Class<?> clazz) throws Exception {
        Object item=  clazz.newInstance();
        Field[] fields = item.getClass().getDeclaredFields(); 
        for(Field field:fields) { 
            Object   arg=map.get(field.getName()); 
            int mod = field.getModifiers();    
            if(Modifier.isStatic(mod) || Modifier.isFinal(mod)||arg==null){    
                continue;    
            }    
            field.setAccessible(true);    
            field.set(item, map.get(field.getName()));  
        }
        return item;
    }


    public static Map<String, Object> objectToMap(Object obj) throws Exception {    
        if(obj == null){    
            return null;    
        }   
        Map<String, Object> map = new HashMap<String, Object>();    
        Field[] declaredFields = obj.getClass().getDeclaredFields();    
        for (Field field : declaredFields) {    
            field.setAccessible(true);  
            map.put(field.getName(), field.get(obj));  
        }    
        return map;  
    }   

}
