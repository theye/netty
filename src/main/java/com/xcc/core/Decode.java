package com.xcc.core;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xcc.core.netty.model.PersonProbuf;
import com.xcc.core.netty.model.PersonProbuf.Person;

public class Decode {
    public static void main(String[] args) throws Exception {
        PersonProbuf.Person.Builder builder = PersonProbuf.Person.newBuilder();
        builder.setEmail("xiaoxiangzi@email.com");
        builder.setId(1);
        builder.setName("筱灬湘子");
        Person person = builder.build();
        byte[] buf = person.toByteArray();
        Person person2 = PersonProbuf.Person.parseFrom(buf);
        System.out.println(person2.getName() + ", " + person2.getEmail());
        System.out.println(buf.length);
    }
}
