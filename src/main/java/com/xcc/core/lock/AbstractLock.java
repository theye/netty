package com.xcc.core.lock;


public abstract class AbstractLock {

    
  
    /**
     * 可以认为是模板模式，两个子类分别实现它的抽象方法
     * 1，简单的分布式锁
     * 2，高性能分布式锁
     */
    public void getLock() {
        String threadName = Thread.currentThread().getName();
        if (tryLock()) {
            System.out.println(threadName+"-获取锁成功");
        }else {
            System.out.println(threadName+"-获取锁失败，进行等待...");
            waitLock();
            //递归重新获取锁
            getLock();
        }
    }
    
    public abstract void releaseLock();
    
    public abstract boolean tryLock();
    
    public abstract void waitLock();
}
