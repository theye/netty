package com.xcc.core.lock;

import com.xcc.core.lock.ZKRWLock.ReadLocK;
import com.xcc.core.lock.ZKRWLock.WriteLocK;

public interface RWLock  {
    
    ReadLocK readLock();
    Lock writeLock();
}
