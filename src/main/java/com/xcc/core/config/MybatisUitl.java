package com.xcc.core.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcc.core.maganer.ManageJdbc;


public class MybatisUitl {
    public static final String key="127.0.0.1:3306";
    private static Logger logger=LoggerFactory.getLogger(MybatisUitl.class);
        static {
            String resource =PropertiesUtil.getValue("mybatis-config");
            logger.debug("mybatis数据库："+resource);
            InputStream inputStream;
            try {
                inputStream = new FileInputStream(System.getProperty("user.dir")+File.separator+resource);
                SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
                ManageJdbc.add(key, sqlSessionFactory);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    
}
