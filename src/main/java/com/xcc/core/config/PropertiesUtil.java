package com.xcc.core.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesUtil {

    public static Properties prop=new Properties();
    static {
        try {
            InputStream   in = new FileInputStream(System.getProperty("user.dir")+"/resources/base/TextTemplate.properties");
            prop.load(in);
            prop.load(new FileInputStream(System.getProperty("user.dir")+"/resources/application-dev.properties"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static Properties getProperties() throws Exception {
        return prop;
    }

    public static String getValue(String key) {
        return prop.getProperty(key)==null?"":prop.getProperty(key);
    }
    public static int getIntValue(String key) {
        return prop.getProperty(key)==null?0:Integer.parseInt(prop.getProperty(key));
    }

    public static void  add(String file)  {
        try {
            InputStream    in = new FileInputStream(file); 
            prop.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
