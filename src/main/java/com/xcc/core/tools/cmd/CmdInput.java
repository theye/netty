package com.xcc.core.tools.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Scanner;

import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;

import com.xcc.core.netty.client.Client;
import com.xcc.core.netty.manager.ClientManager;
import com.xcc.core.tools.Terminal;
import com.xcc.core.tools.type.LocalTerminal;

/*
    命令行工具，可进行远程端操作。
    定义命令集
    []



 */
public class CmdInput {


    
    public static void main(String[] args) throws  Exception {
       Client client= ClientManager.getClient();
       Terminal mClinet = new LocalTerminal();
        Runtime runtime = Runtime.getRuntime();
        Process process;
        process=  runtime.exec("cmd");
        InputStreamReader isr=new InputStreamReader(process.getInputStream(), Charset.forName("GBK"));    
 //       InputStreamReader isr=new InputStreamReader(process.getInputStream());
        BufferedReader br=new BufferedReader(isr);    

      //  echo "你好！齐沁倩"
		new Thread(()->{
            while(true) {
                //用缓冲器读行    
                String line=null;    
                //直到读完为止    
                try {
                    while((line=br.readLine())!=null)    
                    {    
                    	mClinet.send(line.getBytes(Charset.forName("GBK")));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }  
            }

        }).start();
        OutputStreamWriter  out;
        while(true) {
            PrintWriter outputWriter = new PrintWriter(process.getOutputStream(), true);
            outputWriter.println(new String(mClinet.receive()));
        }
    }



}
