package com.xcc.core.tools.type;

import java.awt.Frame;
import java.awt.TextArea;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Scanner;

import com.xcc.core.tools.Terminal;

public class LocalTerminal implements Terminal {
	
	Frame frame=null;
	TextArea tArea=null;
	
	public LocalTerminal() {
		  this.frame=new Frame("Windows窗体");
		 this.tArea=new TextArea();
		 frame.setSize(1080, 1024);
	}
	
	@Override
	public int send(byte[] bs) {
		try {
			tArea.append(new String(bs,"gbk")+"\n" );
		} catch(Exception e) {
			e.printStackTrace();
		}
		frame.add(tArea);
		frame.setVisible(true);//显示窗体
		return 0;
	}

	@Override
	public byte[] receive() {
		  Scanner scan=new Scanner(System.in);
		  byte[] bs=null;
		 try {
			 String msg=scan.nextLine();
	//		 System.out.println(msg);
			 bs =msg.getBytes(Charset.forName("GBK"));
			//		 System.out.println(new String(bs,"gbk"));
		} catch (Exception e) {
			e.printStackTrace();
		}
			return bs;
	}

}
