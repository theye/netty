package com.xcc.core.tools;

public interface Terminal {
    int send(byte[] bs);
    byte[] receive();
}
