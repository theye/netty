package com.xcc.core.maganer;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesMaganer {
    public static Properties prop=new Properties();
    
    public static Properties getProperties() throws Exception {
        return prop;
    }
    public static Properties load(InputStream in) throws Exception {
         prop.load(in);return prop;
    }
    
    public static String getValue(String key) {
        return prop.getProperty(key)==null?"":prop.getProperty(key);
    }
    
    public static int getIntValue(String key) {
        return prop.getProperty(key)==null?0:Integer.parseInt(prop.getProperty(key));
    }
}
