package com.xcc.core.spring.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.xcc.core.spring.model.BaseModel;
import com.xcc.core.spring.service.BaseService;
import com.xcc.core.spring.service.Commservice;

@RestController
public class BaseController {

    @Autowired
    ApplicationContext  mContext;
    @Autowired
    Commservice mCommservice;

    @RequestMapping(value="/comSelect",method = RequestMethod.POST)
    @CrossOrigin
    public Object commselect(@RequestBody JSONObject item) {
        return mCommservice.commselect(item);
    }
    @RequestMapping(value="/commPage",method = RequestMethod.POST)
    @CrossOrigin
    public Object commPage(@RequestBody JSONObject item) {
        return mCommservice.commPage(item);
    }
    @RequestMapping(value="/comUpdate")
    @CrossOrigin
    public void commupdate(@RequestBody JSONObject item) {
        mCommservice.commupdate(item);
    }
    
    @RequestMapping(value="/comInsert",method = RequestMethod.POST)
    @CrossOrigin
    public void  comminsert(@RequestBody JSONObject item) {
        mCommservice.comminsert(item);
    }
    
    @RequestMapping(value="/comDelete")
    @CrossOrigin
    public void  commdelete(@RequestBody JSONObject item) {
        mCommservice.commdelete(item);
    }

    @RequestMapping(value="/postObject",method = RequestMethod.POST)
    public Object postObject(@RequestBody BaseModel args) throws Exception {
        BaseService mBaseService =null;
        try {
            mBaseService = (BaseService)mContext.getBean(args.getBeanName().toUpperCase());
        }catch (Exception e) {
            return "没有服务";
        }
        if(mBaseService==null) {
            return "没有服务";
        }
        return mBaseService.getData(args.getReqArgs());
    }


}
