package com.xcc.core.spring.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlProcessor.ResolutionMethod;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xcc.core.spring.mapper.Commapper;
import com.xcc.core.spring.service.LoginService;
import com.xcc.core.util.Iputils;

@RestController
public class LoginController {

    @Autowired
    Commapper mCommapper;

    @RequestMapping(value="/")
    public Object getLogin() {
        return "ok";
    }

    @RequestMapping(value="ActivationCode")
    public Object getActivationCode(HttpServletRequest request) {
        String uuid=UUID.randomUUID().toString();
        Map<String,Object> mArgs=new HashMap();
        Map<String,Object> entity=new HashMap();
        mArgs.put("tableName", "db_activationCode");
        entity.put("ip", Iputils.getRealIP(request));
        entity.put("ActivationCode", uuid);
        mArgs.put("entity", entity);
        mCommapper.comminsert(mArgs);
        return uuid;
    }



}
