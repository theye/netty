package com.xcc.core.spring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xcc.core.netty.NettyClient;
import com.xcc.core.netty.manager.ManagerChannel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;

@RestController
public class StartController {
    
    
    NettyClient mNettyClient1=new NettyClient("127.0.0.1",10087);
    NettyClient mNettyClient2=new NettyClient("127.0.0.1",10087);
    
    
    @RequestMapping("startClient1")
    public void start1() throws Exception {
        mNettyClient1.start();
    }
    
    @RequestMapping("startClient2")
    public void start2() throws Exception {
        mNettyClient2.start();
    }
    
    
    //客户1端发送
   @RequestMapping("sendClient1")
    public Object send1(String data) {
        JSONObject sendMsg=(JSONObject)JSONObject.parse(data);
        String keyChannel=sendMsg.getString("client");
        System.out.println(keyChannel);
        Channel  ch=ManagerChannel.getServerChannel(keyChannel);
        ByteBuf buf = Unpooled.buffer(data.length());
        buf.writeBytes(data.getBytes());
        ch.writeAndFlush(buf);
        return data;
    }
    
   //客户2端发送
    @RequestMapping("sendClient2")
    public Object send2(String data) {
        JSONObject sendMsg=(JSONObject)JSONObject.parse(data);
        String keyChannel=sendMsg.getString("client");
        System.out.println(keyChannel);
        Channel  ch=ManagerChannel.getServerChannel(keyChannel);
        ByteBuf buf = Unpooled.buffer(data.length());
        buf.writeBytes(data.getBytes());
        ch.writeAndFlush(buf);
        return data;
    }
    
    @RequestMapping("getAllClient")
    public Object getClient() {
        return ManagerChannel.getAllServer();
    }
    
}
