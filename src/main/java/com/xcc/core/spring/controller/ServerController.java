package com.xcc.core.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xcc.core.netty.NettyLengthServer;
import com.xcc.core.spring.model.Server;

@RestController
public class ServerController {

    private final static Logger log=LoggerFactory.getLogger(ServerController.class);

    NettyLengthServer mServer=new NettyLengthServer();


    @RequestMapping(value="/start")
    public Object stater(@RequestBody Server server) {
        new Thread(()->{
            try {
                mServer.startServer(
                    server.getPort(),
                    server.getIp(),
                    server.getProxyport(),
                    server.getChannlId());
            } catch (Exception e) {
                log.error(e.getMessage(),e);
            }
        }).start();
        return "ok";
    }

}

