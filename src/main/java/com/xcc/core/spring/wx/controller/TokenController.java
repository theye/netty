package com.xcc.core.spring.wx.controller;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xcc.core.config.PropertiesUtil;
import com.xcc.core.spring.wx.util.SecuritySHA1Utils;
import com.xcc.core.spring.wx.util.WXBizMsgCrypt;

import io.netty.handler.codec.http.HttpRequest;

@RestController
public class TokenController {

	private static final Logger logger=LoggerFactory.getLogger(TokenController.class);

	@RequestMapping(value="/api/message",method = RequestMethod.GET)
	public String authToken(String signature,
			String timestamp,
			String nonce,
			String echostr) {
		try {
			logger.debug("signature:"+signature+"timestamp:"+timestamp+"nonce:"+nonce+"echostr:"+echostr);
			String[] args=new String[3];
			args[0]=PropertiesUtil.getValue("wx.token");
			args[1]=timestamp;
			args[2]=nonce;
			SecuritySHA1Utils.sort(args);
			String   signature1=   SecuritySHA1Utils.shaEncode(args[0]+args[1]+args[2]);
			if(signature1.equals(signature)) {
				return echostr;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/api/message",method = RequestMethod.POST)
	public String  authToken(HttpServletRequest request,@RequestBody String xml) {
		try {
			System.out.println(xml);
			String msgSignature= request.getParameter("msg_signature");
			String   nonce=    request.getParameter("nonce");
			String   timeStamp= request.getParameter("timestamp");
			System.out.println();
			//      Document  doc = DocumentHelper.parseText(xml);

			//    Element rootElt = doc.getRootElement(); // 获取根节点
		//	System.out.println( doc.getRootElement().element("ToUserName").getText());
			//          String     postData=doc.getRootElement().element("Encrypt").getText();
			//      System.out.println( doc.getRootElement().element("FromUserName").getText());
			//       System.out.println( doc.getRootElement().element("CreateTime").getText());
			//       System.out.println( doc.getRootElement().element("MsgType").getText());
			//     System.out.println( doc.getRootElement().element("Content").getText());

			WXBizMsgCrypt mWXBizMsgCrypt=new  WXBizMsgCrypt(
					PropertiesUtil.getValue("wx.token") ,
					PropertiesUtil.getValue("wx.aeskey"),
					PropertiesUtil.getValue("wx.appid"));
			String postdata=mWXBizMsgCrypt.decryptMsg(msgSignature,timeStamp,nonce,xml);
			System.out.println(postdata);
			Document  doc = DocumentHelper.parseText(postdata);
			System.out.println(doc);
			System.out.println(doc.getRootElement().element("ToUserName").getText());
			System.out.println( doc.getRootElement().element("FromUserName").getText());
			System.out.println( doc.getRootElement().element("CreateTime").getText());
			System.out.println( doc.getRootElement().element("MsgType").getText());
			System.out.println( doc.getRootElement().element("Content").getText());
			System.out.println( doc.getRootElement().element("MsgId").getText());
		} catch ( Exception e) {
			e.printStackTrace();
		}  
		return "success";

	}

}
