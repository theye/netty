package com.xcc.core.spring.wx.client;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;

import com.xcc.core.config.PropertiesUtil;

public class Wxclient {
    
    @Value("${wx.appid}")
    private String appid;
    @Value("${wx.appsecret}")
    private String appsecret;
    @Value("${wx.grant_type}")
    private String grant_type;
 
    public static String getToken() throws Exception {
        System.out.println(PropertiesUtil.getValue("wx.token.url"));
        Document   mDocument =  Jsoup.connect(PropertiesUtil.getValue("wx.token"))
        .ignoreContentType(true)
        .get();
        return mDocument.body().ownText();
    }
    
    
    public static void main(String[] args) throws Exception {
        System.out.println(getToken());
    }
  
    
}
