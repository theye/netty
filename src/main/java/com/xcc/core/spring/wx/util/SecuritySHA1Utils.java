package com.xcc.core.spring.wx.util;

import java.security.MessageDigest;

import org.springframework.beans.factory.annotation.Value;

public class SecuritySHA1Utils {
    
   
    @Value("${wx.token}")
    private String token;
    
    /**
     * @Comment SHA1实现
     * @Author Ron
     * @Date 2017年9月13日 下午3:30:36
     * @return
     */
    public static String shaEncode(String inStr) throws Exception {
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-1");
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
 
        byte[] byteArray = inStr.getBytes("UTF-8");
        byte[] md5Bytes = sha.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }
    
    public static void sort(String a[]) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[j].compareTo(a[i]) < 0) {
                    String temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    } 
    
    
    
 //token、timestamp、nonce
    public static void main(String args[]) throws Exception {
        String token="15410078001620278262snsknlansdand42d2932han";
    //    String str = new String("amigoxiexiexingxing");
     //   System.out.println("原始：" + str);
        System.out.println("SHA后：" + shaEncode(token));
    }

}
