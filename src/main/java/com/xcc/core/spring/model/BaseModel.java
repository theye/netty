package com.xcc.core.spring.model;


import com.alibaba.fastjson.JSONObject;

public class BaseModel {

    private String beanName;
    
    
    private JSONObject reqArgs;


    public String getBeanName() {
        return beanName;
    }


    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }


    public JSONObject getReqArgs() {
        return reqArgs;
    }

    public void setReqArgs(JSONObject reqArgs) {
        this.reqArgs = reqArgs;
    }
    
}
