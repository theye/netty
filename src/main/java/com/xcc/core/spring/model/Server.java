package com.xcc.core.spring.model;

public class Server {
    public int port;
    public String ip;
    public int proxyport;
    public String channlId;
    
    public int getPort() {
        return port;
    }
    
    public void setPort(int port) {
        this.port = port;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public int getProxyport() {
        return proxyport;
    }
    public void setProxyport(int proxyport) {
        this.proxyport = proxyport;
    }
    public String getChannlId() {
        return channlId;
    }
    public void setChannlId(String channlId) {
        this.channlId = channlId;
    }
    
}
