package com.xcc.core.spring.dao;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.xcc.core.spring.mapper.Commapper;

public class BaseDao<T extends Commapper,M> {
    @Autowired
    T mCommapper;

    public List<M> getListObejct(JSONObject list,Class<?> clazz) throws Exception{
        List<Map> mlist=mCommapper.commselect(list);
        List<Object> mOs=new ArrayList<Object>();
        for(Map map:mlist) {
            mOs.add(tran(map,clazz));
        }
        return (List<M>)mOs;
    }

    public void commupdate(Map map) {
        mCommapper.commupdate(map);
    }
    public void comminsert(Map map) {
        mCommapper.comminsert(map);
    }
    
    public void commdelete(Map map) {
        mCommapper.commdelete(map);
    }
    public static Object tran(Map map,Class<?> clazz) throws Exception {
        Object item=  clazz.newInstance();
        Field[] fields = item.getClass().getDeclaredFields(); 
        for(Field field:fields) { 
            Object   arg=map.get(field.getName()); 
            int mod = field.getModifiers();    
            if(Modifier.isStatic(mod) || Modifier.isFinal(mod)||arg==null){    
                continue;    
            }    
            field.setAccessible(true);    
            field.set(item, map.get(field.getName()));  
        }
        return item;
    }
    
}
