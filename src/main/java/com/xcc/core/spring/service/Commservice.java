package com.xcc.core.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.xcc.core.spring.mapper.Commapper;
@Service
public class Commservice {
    @Autowired
    Commapper mCommapper;
    
    public Object commselect(JSONObject item) {
        return  mCommapper.commselect(item);
    }
    //读取redis 映射
    public Object commPage(JSONObject item) {
        JSONObject data=new JSONObject();
        Integer pageSize=item.getInteger("pageSize");
        pageSize=StringUtils.isEmpty(pageSize)?20:pageSize;
        Integer currentPage=item.getInteger("currentPage");
        currentPage=StringUtils.isEmpty(currentPage)?1:currentPage;
        item.put("currentPage",pageSize*(currentPage-1));
        item.put("pageSize",pageSize);
        List mdata=mCommapper.commPage(item);
        data.put("data",mdata);
        int size=mCommapper.commtotal(item);
        data.put("total",size);
        data.put("currentPage",currentPage);
        data.put("pageSize",pageSize);
        return  data;
    }

    public void commupdate(JSONObject item) {
          mCommapper.commupdate(item);
    }

    public void comminsert(JSONObject item) {
          mCommapper.comminsert(item);
        
    }

    public void commdelete(JSONObject item) {
          mCommapper.commdelete(item);
    }
    
}
