package com.xcc.core.spring.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.alibaba.fastjson.JSONObject;

public interface BaseService {
	public Object getData();
	public Object getData(JSONObject args) throws Exception;
	public Object getData(JSONObject args,HttpServletRequest req,HttpServletResponse repon) throws Exception;
}
