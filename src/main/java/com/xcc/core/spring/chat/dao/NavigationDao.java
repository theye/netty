package com.xcc.core.spring.chat.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xcc.core.spring.chat.mapper.NavigationMapper;
import com.xcc.core.spring.chat.model.Navigation;
import com.xcc.core.spring.dao.BaseDao;

@Component
public class NavigationDao extends BaseDao<NavigationMapper,Navigation> {
    
    @Autowired
    NavigationMapper mNavigationMapper;
    
    
}
