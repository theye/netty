package com.xcc.core.spring.chat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xcc.core.spring.chat.service.NavigationService;

@RestController
public class NavigationController {
    @Autowired
    NavigationService mBaseService;
    
    
    @RequestMapping(value="navigation",method = RequestMethod.POST)
    @CrossOrigin
    public Object getnavigation() throws Exception {
        return mBaseService.getData();
    }
    
     
}
