package com.xcc.core.spring.chat.model;

import java.util.Date;
import java.util.List;

public class Navigation {

    private Integer id;
    private String label;
    private Date createtime;
    private Date updatetime;
    private Integer parentId;
    private String route;
    private String icon;
    private List<Navigation> subs;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public void setLabel(String label) {
         this.label = label;
     }
     public String getLabel() {
         return label;
     }
    
    public Date getCreatetime() {
        return createtime;
    }
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getUpdatetime() {
        return updatetime;
    }
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
   
    public Integer getParentId() {
        return parentId;
    }
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    public void setRoute(String route) {
         this.route = route;
     }
     public String getRoute() {
         return route;
     }
    public List<Navigation> getSubs() {
        return subs;
    }
    public void setSubs(List<Navigation> subs) {
        this.subs = subs;
    }
    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    
}
