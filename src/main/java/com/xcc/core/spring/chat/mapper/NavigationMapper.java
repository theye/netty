package com.xcc.core.spring.chat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.xcc.core.spring.chat.model.Navigation;
import com.xcc.core.spring.mapper.Commapper;

@Mapper
public interface NavigationMapper extends Commapper{

    public List<Navigation> getnavigationlist();
    
    
}
