package com.xcc.core.spring.chat.service;

public interface BaseService {
    public Object getData() throws Exception;
}
