package com.xcc.core.spring.chat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.xcc.core.spring.chat.dao.NavigationDao;
import com.xcc.core.spring.chat.mapper.NavigationMapper;
import com.xcc.core.spring.chat.model.Navigation;
import com.xcc.core.spring.mapper.Commapper;

@Service
public class NavigationService implements BaseService{
    
    private static final String key="Navigation_";
    
    @Autowired
    NavigationDao mNavigationDao;
    
    
    
    @Override
    public Object getData() throws Exception {
        HashMap<String,Object> tableName=new HashMap();
        tableName.put("tableName", "db_navigation");
        tableName.put("userId", 1);
        HashMap<String,Object> item=new HashMap();
        tableName.put("item", item);
        JSONObject mj=new JSONObject(tableName);
       return  getChilds(mNavigationDao.getListObejct(mj,Navigation.class));
    }

    @SuppressWarnings("deprecation")
    public List<Navigation> getChilds(List<Navigation> mlabels) {
        List<Navigation> mChilds=new  ArrayList<Navigation>();
        for(Navigation temp:mlabels) {
            if(StringUtils.isEmpty(temp.getParentId())) {
                temp.setSubs(getChild(temp,mlabels));
                mChilds.add(temp);
            } 
        }
        return mChilds;
    }

    
    public List<Navigation> getChild(Navigation temp,List<Navigation>  mlabels) {
        List<Navigation> mList=new ArrayList<Navigation>();
        for(Navigation navigation:mlabels) {
            if(temp.getId()==navigation.getParentId()) {
                mList.add(navigation); 
                List<Navigation> subs=getChild(navigation,mlabels);
                if(subs!=null&&!subs.isEmpty()) {
                    navigation.setSubs(subs);
                }
            }  
        }
        //返回null
        if(mList.isEmpty()) {
            return null;
        }
        return mList;
    }
}
