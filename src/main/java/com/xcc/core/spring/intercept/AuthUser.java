package com.xcc.core.spring.intercept;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import com.alibaba.fastjson.JSONObject;
import com.xcc.core.spring.mapper.Commapper;
import com.xcc.core.util.Iputils;
import com.xcc.core.util.bean.TableBeanFactory;

public class AuthUser implements HandlerInterceptor{

    @Autowired
    Commapper mCommapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler)
                                 throws Exception {
        Map<String,Object> mIPs= TableBeanFactory.getCom("db_auth");
        String ip=Iputils.getRealIP(request);
        Map<String,Object> ips;
        if( mIPs.get("entity") instanceof Map) {
            ips= (Map) mIPs.get("entity") ;
            ips.put("ip", ip);
            JSONObject enity=  new JSONObject(mIPs);
            List a=  mCommapper.commselect(enity);
            if(a.size()>0) return true;
        }
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.getWriter().write(new String("无权限".getBytes(),"utf-8"));
        return false;
    }
  
}
