第一步 生成服务器端私钥和证书仓库命令
keytool -genkey -alias mySrvAlias1 -keysize 2048 -validity 365 -keyalg RSA -dname "CN=localhost" -keypass skeypass123 -storepass sstorepass456 -keystore yqServer.jks

-keysize: 2048 密钥长度2048位（这个长度的密钥目前可认为无法被暴力破解）
-validity: 365 证书有效期365天，测试中365就高了，实际生产中我们会冲认证机构获取证书，有效期比较长
-keyalg: RSA 使用RSA非对称加密算法
-dname: "CN=localhost" 设置Common Name为localhost
-keypass: skeypass123 密钥的访问密码为skeypass123 
-storepass: sstorepass456 密钥库的访问密码为sstorepass456 
-keystore: sChat.jks 指定生成的密钥库文件为sChata.jks


第二步 生成服务器端自签名证书
keytool -export -alias mySrvAlias1 -keystore yqServer.jks -storepass sstorepass456 -file yqServer.cer

第三步 ：生成客户端的密钥对和证书仓库，用于将服务器端的证书保存到客户端的授信证书仓库中
keytool -genkey -alias myClientAlias1 -keysize 2048 -validity 365 -keyalg RSA -dname "CN=localhost" -keypass ckeypass987 -storepass cstorepass654 -keystore yqClient.jks

第四步 ：将服务器端证书导入到客户端的证书仓库中
keytool -import -trustcacerts -alias mySrvAlias1 -file yqServer.cer -storepass cstorepass654 -keystore yqClient.jks

如果只做单向认证，到此就可以结束了，如果是双响认证，则还需第五步和第六步

第五步 生成客户端自签名证书
keytool -export -alias myClientAlias1 -keystore yqClient.jks -storepass cstorepass654 -file yqClient.cer

第六步 将客户端的自签名证书导入到服务器端的信任证书仓库中：
keytool -import -trustcacerts -alias myClientSelfAlias -file yqClient.cer -storepass sstorepass456 -keystore yqServer.jks

到此，证书就生成完毕了，我们就可以得到两个jks文件，一个是服务端的yqServer.jks ，一个是客户端的yqClient.jks , 两个cer文件yqServer.cer和yqClient.cer

单向认证与双向认证区别：
与单向认证不同的是， 双向认证中，服务端也需要对客户端进行安全认证，这就意味着客户端的自签名证书也需要导入到服务器的数组证书仓库中。
我们一般使用https，都是单向认证，就是我们详细该网站就是可信任的网站，不是伪造假冒的。
我们使用网上银行或者一些需要高安全性的服务时需要双向认证，因为有U盾之类的东西，银行或者其他需要高安全性的服务已经将颁发给我们的证书添加到自己的信任列表中了。

