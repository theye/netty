@echo off
set LIB_JARS="F:\netty\lib\*"
set RUN_JARS="F:\netty\target\classes"
java -Dfile.encoding=UTF-8  ^
-XX:+HeapDumpOnOutOfMemoryError ^
-XX:HeapDumpPath=./prof/netty.prof ^
-classpath %RUN_JARS%;%LIB_JARS%  ^
-Xmx50m ^
com.xcc.core.spder.test.langchao.SpderLangchao
goto end